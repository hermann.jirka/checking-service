package cz.jh.proj.checkingservice.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import cz.jh.proj.checkingservice.dto.UrlRequestDto
import cz.jh.proj.checkingservice.service.CheckUrlService
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class CheckUrlServiceImplTest {
    @Autowired
    lateinit var checkUrlService: CheckUrlService

    private val objectMapper = ObjectMapper().registerKotlinModule()

    @Test
    fun checkUrlsV1() = runBlocking {
        val bodyUrlsString = this::class.java.classLoader.getResource("urls/url.json")
        val request = objectMapper.readValue(bodyUrlsString, UrlRequestDto::class.java)

        val response = checkUrlService.checkUrlV1(request.urls)

        Assertions.assertEquals(5, response.size)
    }

    @Test
    fun checkUrlsV2() = runBlocking {
        val bodyUrlsString = this::class.java.classLoader.getResource("urls/url.json")
        val request = objectMapper.readValue(bodyUrlsString, UrlRequestDto::class.java)

        val response = checkUrlService.checkUrlV2(request.urls).toList()

        Assertions.assertEquals(5, response.size)
    }

}
