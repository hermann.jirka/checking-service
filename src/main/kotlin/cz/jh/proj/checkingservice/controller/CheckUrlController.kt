package cz.jh.proj.checkingservice.controller

import cz.jh.proj.checkingservice.dto.CheckedUrlDto
import cz.jh.proj.checkingservice.dto.UrlRequestDto
import cz.jh.proj.checkingservice.service.CheckUrlService
import io.swagger.v3.oas.annotations.Operation
import kotlinx.coroutines.flow.Flow
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/check")
class CheckUrlController(
    private val checkUrlService: CheckUrlService
) {
    @Operation(description = "First method uses own extension for run 3 threads in parallel, there is used semaphore")
    @PostMapping("/urls/v1")
    suspend fun checkUrlV1(
        @RequestBody request: UrlRequestDto
    ): List<CheckedUrlDto> {
        return checkUrlService.checkUrlV1(request.urls)
    }

    @Operation(description = "Second method uses flow for run 3 threads in parallel")
    @PostMapping("/urls/v2")
    suspend fun checkUrlV2(
        @RequestBody request: UrlRequestDto
    ): Flow<CheckedUrlDto> {
        return checkUrlService.checkUrlV2(request.urls)
    }
}