package cz.jh.proj.checkingservice.dto

data class UrlRequestDto(
    val urls: Set<String>
)

data class CheckedUrlDto(
    val url: String,
    val status: Status,
)

enum class Status {
    AVAILABLE, UNAVAILABLE
}
