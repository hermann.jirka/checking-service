package cz.jh.proj.checkingservice.service.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Semaphore

/**
 * Kotlin's coroutines implementation of semaphore, way how we can implement n threads to run parallel
 */
suspend fun <T, R> Iterable<T>.semaphore(
    concurrency: Int,
    transform: suspend (T) -> R
): List<R> = runBlocking {
    val semaphore = Semaphore(permits = concurrency)
    map { item ->
        semaphore.acquire()
        async(Dispatchers.Default) {
            try {
                transform(item)
            } finally {
                semaphore.release()
            }
        }
    }.awaitAll()
}
