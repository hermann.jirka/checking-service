package cz.jh.proj.checkingservice.service.impl

import cz.jh.proj.checkingservice.dto.CheckedUrlDto
import cz.jh.proj.checkingservice.dto.Status.AVAILABLE
import cz.jh.proj.checkingservice.dto.Status.UNAVAILABLE
import cz.jh.proj.checkingservice.service.CheckUrlService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.awaitFirst
import org.apache.commons.validator.routines.UrlValidator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.net.URI
import java.util.concurrent.Executors

@Service
class CheckUrlServiceImpl(
    private val webClient: WebClient
) : CheckUrlService {
    /**
     * First version of check url v1, it is used extension semaphore for run 3 threads
     */
    override suspend fun checkUrlV1(urls: Set<String>): List<CheckedUrlDto> {
        logger.info("Check urls is available: ${urls.size}")
        val validUrls = validationUrls(urls)
        return validUrls.semaphore(concurrency = 3) { callWebClient(it) }.toList()
    }

    /**
     * Second version of check url v2, it is used flow with executors new fixed thread pool for run 3 threads
     */
    override suspend fun checkUrlV2(urls: Set<String>): Flow<CheckedUrlDto> {
        logger.info("Check urls is available: ${urls.size}")
        val threadCount = 3
        val dispatcher = Executors.newFixedThreadPool(threadCount).asCoroutineDispatcher()
        val validUrls = validationUrls(urls)

        return validUrls.asFlow()
            .map { CoroutineScope(dispatcher).async { callWebClient(it) } }
            .buffer(threadCount)
            .map { it.await() }
            .flowOn(dispatcher)
    }

    private suspend fun validationUrls(urls: Set<String>): Set<String> {
        val schema = arrayOf("http", "https")
        val validator = UrlValidator(schema)
        return urls.filter { t -> validator.isValid(t) }.toSet()
    }

    /**
     * Checking if status code in response is 2xx or not
     */
    private suspend fun callWebClient(url: String): CheckedUrlDto {
        return webClient.get()
            .uri(URI.create(url))
            .exchangeToMono { response ->
                if (response.statusCode().is2xxSuccessful) {
                    Mono.just(CheckedUrlDto(url = url, status = AVAILABLE))
                } else {
                    Mono.just(CheckedUrlDto(url = url, status = UNAVAILABLE))
                }
            }
            .awaitFirst()
    }

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }
}