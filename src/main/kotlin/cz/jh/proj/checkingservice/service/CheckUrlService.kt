package cz.jh.proj.checkingservice.service

import cz.jh.proj.checkingservice.dto.CheckedUrlDto
import kotlinx.coroutines.flow.Flow

interface CheckUrlService {
    suspend fun checkUrlV1(urls: Set<String>): List<CheckedUrlDto>
    suspend fun checkUrlV2(urls: Set<String>): Flow<CheckedUrlDto>
}