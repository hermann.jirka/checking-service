package cz.jh.proj.checkingservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CheckingServiceApplication

fun main(args: Array<String>) {
	runApplication<CheckingServiceApplication>(*args)
}
