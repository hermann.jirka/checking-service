# Checking-service

## Task

Create a simple web application in Kotlin, that will receive a POST request with JSON body (list of urls). The
application will then test the availability of all URLs and return JSON with a testing result. Do the testing in
parallel (using max 3 threads). Please use Spring or Spring Boot and WebFlux + any other technology/framework if needed.

## Service

Running on port 8991

## Local test

For local testing there is in folder http file, we can use it for run http requests

## Swagger

Running on url: http://localhost:8991/webjars/swagger-ui/index.html

